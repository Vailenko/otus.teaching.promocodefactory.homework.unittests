﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builder.Domain;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builder.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;
using AutoFixture;
using AutoFixture.AutoMoq;
using System.Linq;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        ////TODO: Add Unit Tests
        // 1 - Если партнер не найден, то также нужно выдать ошибку 404;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = PartnerBuilder.CreateBase().Id;
            var request = SetPartnerPromoCodeLimitRequestBuilder.CreateBase();
            Partner partner = null;
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }
        //2 - Если партнер заблокирован, то есть поле IsActive = false в классе Partner, то также нужно выдать ошибку 400;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBase().Inactive();
            var request = SetPartnerPromoCodeLimitRequestBuilder.CreateBase();
           
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
           
        }
        //3.1 -Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitSetForPartnerWithActiveLimits_NumberIssuedPromoCodesEqualZero()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBase();
            var request = SetPartnerPromoCodeLimitRequestBuilder.CreateBase();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
           
        }
        //3.2 -Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, 
        //если лимит закончился, то количество не обнуляется
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitSetForPartnerWithoutActiveLimits_NumberIssuedPromoCodesNotEqualZero()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBase().WithoutActiveLimits();
            var request = SetPartnerPromoCodeLimitRequestBuilder.CreateBase();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().NotBe(0);
        }
        //4 - При установке лимита нужно отключить предыдущий лимит;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_CurrentLimitCancelDate_NotNull()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBase();
            var request = SetPartnerPromoCodeLimitRequestBuilder.CreateBase();
            var currentLimit = partner.GetCurrentLimit();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            currentLimit.CancelDate.Should().NotBeNull();
        }
        // 5 - Лимит должен быть больше 0
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitLessEqualZero_ReturnsBadRequest()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBase();
            var request = SetPartnerPromoCodeLimitRequestBuilder.CreateBase().WithLimitZero();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();

        }
        // 6 Нужно убедиться, что сохранили новый лимит в базу данных (это нужно проверить Unit-тестом)
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_BaseRequest_AddIsInvoked()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBase();
            var request = SetPartnerPromoCodeLimitRequestBuilder.CreateBase();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //assert
            _partnersRepositoryMock.Verify(repo => repo.UpdateAsync(partner), Times.Once);
        }
    }
}