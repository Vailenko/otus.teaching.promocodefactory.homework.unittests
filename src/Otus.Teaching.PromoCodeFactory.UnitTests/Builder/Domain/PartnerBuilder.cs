﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builder.Domain
{
    public static class PartnerBuilder
    {
        public static Partner CreateBase()
        {
            return new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                NumberIssuedPromoCodes = 2,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };
        }
        public static Partner Inactive(this Partner partner)
        {
            partner.IsActive = false; 
            return partner;
        }
        public static Partner WithoutActiveLimits(this Partner partner)
        {
            foreach ( var limit in partner.PartnerLimits)
            {
                limit.CancelDate = new DateTime(2021, 02, 22);
            }
            return partner;
        }
        public static PartnerPromoCodeLimit GetCurrentLimit(this Partner partner)
        {
            var limit = partner.PartnerLimits.FirstOrDefault();
            return limit;
        }
    }
   
}
