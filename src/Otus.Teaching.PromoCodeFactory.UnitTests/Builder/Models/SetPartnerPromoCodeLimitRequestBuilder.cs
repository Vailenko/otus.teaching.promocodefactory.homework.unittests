﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builder.Models
{
    public static class SetPartnerPromoCodeLimitRequestBuilder
    {
        public static SetPartnerPromoCodeLimitRequest CreateBase()
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = new DateTime(2021, 12, 31),
                Limit = 1
            };
        }
        public static SetPartnerPromoCodeLimitRequest WithLimitZero(this SetPartnerPromoCodeLimitRequest request)
        {
            request.Limit = 0;
            return request;
        }
    }
}
